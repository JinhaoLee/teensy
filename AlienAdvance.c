/*  CAB202 Assignment 2
*   
*   Jinhao Li, May 2017
*   n9504508
*   Queensland University of Technology
*/

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <math.h>
#include "lcd.h"
#include "graphics.h"
#include "cpu_speed.h"
#include "sprite.h"
#include "usb_serial.h"

#define FREQUENCY 8000000.0
#define PRESCALER 64.0
#define PRESCALER_1 1024.0
volatile unsigned long ovf_count = 0;
volatile double seconds;
int minutes;
int mothership_fight = 0;
int scores = 0;
#define MAX_LIVES 5
int lives = MAX_LIVES;
int health_pool = 10;
int display_info = 0;

void init_hardware(void);
void process(void);
void timer_init(void);
void setup_menu(void);
void setup_spacecraft(void);
void setup_game_board(void);
void directional_pad_control(void);
void launch_missiles(void);
void setup_aliens(void);
void draw_aliens(void);
void update_missiles(void);
void update_aliens(void);
void setup_missiles(void);
void dash_attack(int i, int v);
void sprite_move(Sprite sprite, float dx, float dy );
void aliens_hit_board();
void draw_missiles();
void missiles_turn_up();
void check_collision();
void check_collision2();
void resetup_aliens();
void aliens_rematerialise();
double get_system_time();
void game_over();
int sprites_collided(Sprite sprite_1, Sprite sprite_2);
void sprite_collided_with_any(Sprite sprite, Sprite sprites[], int num_sprites);
int sprites_collided2(Sprite sprite_1, Sprite sprite_2);

void send_debug_string(char* string);
void draw_centred(unsigned char y, char* string);
void enter_breakpoint();
void send_spacecraft_information();
int16_t read_adc(int8_t pin_number);
int16_t my_adc_value;
void aiming_system(void);
void setup_motherShip();
void check_collision3();
void motherShip_misile();
void dash_attack2(int v);
void send_debug_string1(char* string);

Sprite spacecraft;
#define PLAYER_WIDTH 8
#define PLAYER_HEIGHT 5
unsigned char player_image[5] = {
    0b00011000,
    0b01111110,
    0b11111111,
    0b10111101,
    0b11000011,
};

#define MAX_ALIENS 6
#define ALIEN_VELOCITY 9
#define MOTHERSHIP_VELOCITY 6
Sprite aliens[MAX_ALIENS];
unsigned char alien_image[5] = {
    0b00011000,
    0b10111101,
    0b01100110,
    0b10111101,
    0b00011000,
};

int aliens_move[MAX_ALIENS] = {0};
float aliens_move_to[MAX_ALIENS] = {7.0,7.5,8.0,8.5,9.0};

#define MAX_MISSILES 5
#define MISSILE_VELOCITY 1
Sprite missiles[MAX_MISSILES];
Sprite mothership_missile;
unsigned char missile_image[2] = {
    0b11111111,
    0b11111111,
};
float direction = 0;
float missiles_dir[MAX_MISSILES] = {0,0,0,0,0};

Sprite motherShip;
unsigned char motherShip_image[20] = {
    0b11111111,0b11000000,
    0b11100001,0b11000000,
    0b11010010,0b11000000,
    0b11001100,0b11000000,
    0b11001100,0b11000000,
    0b11010010,0b11000000,
    0b11100001,0b11000000,
    0b11111111,0b11000000,
    0b00000000,0b00000000,
    0b11111111,0b11000000,
};

//wirte the main file
int main(void) {

    // Set the CPU speed to 8MHz (you must also be compiling at 8MHz)
    set_clock_speed(CPU_8MHz);
    
    srand(get_system_time());
    
    // Setup the hardware
    init_hardware();

    // Wait until the 'debugger' is attached...
    draw_centred(17, "Waiting for");
    draw_centred(24, "debugger...");
    show_screen();
    
    while(!usb_configured() || !usb_serial_get_control());
    send_debug_string1("CAB202: Assignment 2 - 2017");
    send_debug_string1("Welcome to the USB debugger");
    send_debug_string1("The serial communication channel is active");
    send_debug_string1("Move player: W (up) A (left) S (down) D (right)");
    send_debug_string1("Shoot Bullets: spacebar");
    

    clear_screen();
    draw_centred(10, "Teensy is");
    draw_centred(20, "connected via");
    draw_centred(30, "USB");
    show_screen();
    _delay_ms(2000);
    
    //initialiase timer
    timer_init();
    
    clear_screen();
    
    // Setup the menu
    setup_menu();
    ovf_count = 0;
    show_screen();  
    
    // prcess the game
    process();
    
    return 0;
}

void process() {
    while(1) {
        // clear screen
        clear_screen();
        enter_breakpoint();
        // button control
        directional_pad_control();

        // setup game board
        setup_game_board();

        // draw player
        draw_sprite(&spacecraft);
        
        // update missiles
        aiming_system();
        launch_missiles();
        update_missiles();
        draw_missiles();

        // update and draw aliens
        update_aliens();
        aliens_hit_board();
        draw_aliens();
        aliens_rematerialise();
        check_collision();
        check_collision2();
        check_collision3();
      
        show_screen();
        if (lives <= 0) {
            game_over();
        }
    }
}
void init_hardware(void) {
    // Initialising the LCD screen
    lcd_init(LCD_DEFAULT_CONTRAST);

    // Initalising the buttons as inputs
    DDRF &= ~((1 << PF5) | (1 << PF6));
    DDRB &= ~(1<<1) | ~(1<<7); // Left and Down
    DDRD &= ~(1<<0) | ~(1<<1); // Right and Up

    // Initialising the LEDs as outputs
    DDRB |= ((1 << PB2) | (1 << PB3));

    usb_init();

    seconds = 0;
    minutes = 0;

    // AREF = AVcc
    ADMUX = (1<<REFS0);
 
    // ADC Enable and pre-scaler of 128
    // 8000000/128 = 62500
    ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
}


// Initailise the timer
void timer_init(void)
{
    // Configure all necessary timers in "normal mode", enable all necessary
    // interupts, and configure prescalers as desired
    TCCR1B &= ~((1<<WGM02));
    // Globally enable interrupts
    /*
    TCCR1B |= (1<<CS02)|(1<<CS00);
    TCCR1B &= ~((1<<CS01));
    */
    // 0.524288
    TCCR1B &= ~((1<<CS02));
    TCCR1B |= (1<<CS01)|(1<<CS00);
    TIMSK1 |= (0b1 << TOIE1);
     // Globally enable interrupts
    sei();

}

void setup_menu() { 
	display_info = 0;
    int quit_menu = 0;
    int countdown = 3;
    
    while(!quit_menu) {
        draw_centred(0,"Alien Advance!");
        draw_centred(10,"Jinhao Li");
        draw_centred(20,"n9504508");
        draw_centred(30,"Press any button");
        draw_centred(40, "to start...");
        show_screen();
        
        if (((PINF>>5) & 0b1) || ((PINF>>6) & 0b1)) {
            quit_menu = 1;
            lives = MAX_LIVES;
            scores = 0;
            direction = 0;
            health_pool = 10;
            setup_aliens();
            setup_spacecraft();
            setup_missiles();
            setup_motherShip();
            mothership_fight = 0;
            motherShip_image[18] = 0b11111111;     
            motherShip_image[19] = 0b11000000;
            for (unsigned int i = 0; i<MAX_ALIENS;i++){
                aliens_move[i] = 0;
            }
        }
    }   

    char buff[80];
    
    while (countdown >= 0) {
        clear_screen();
	    if (countdown != 0) {
	        sprintf(buff, "%d", countdown);
	        draw_string(40,20,buff);
	    } else {
	      draw_string(17,20,"Game starts!");
	    }
	        show_screen();
	        _delay_ms(300);
	        countdown--;
    }
    seconds = 0;
    minutes = 0;
    display_info = 1;
}

void setup_game_board() {
    char buff[80];
    char buff1[80];
    char buff2[80];

    sprintf(buff, "S:%d", scores);
    draw_string(2,1,buff);
    
    sprintf(buff2, "T:%02d:%02d", minutes, (int) floor(seconds));
    draw_string(LCD_X / 2 + 5,1,buff2);

    sprintf(buff1, "L:%d", lives);
    draw_string(LCD_X / 2 - 17,1,buff1);

    draw_line(0, 0, LCD_X - 1, 0);
    draw_line(0, 8, LCD_X - 1, 8);
    draw_line(LCD_X - 1, 0, LCD_X - 1, LCD_Y);
    draw_line(0, 0, 0, LCD_Y);
    draw_line(0, LCD_Y - 1, LCD_X - 1, LCD_Y - 1);
}

void game_over() {
	display_info = 0;
	char buff[80];
    clear_screen(); 
    int quit = 0;
   	
   	while(!quit) {
	    draw_centred(5," GAME OVER!");
	    draw_centred(15,"The Score was");
	    sprintf(buff,"%d", scores);
	    draw_centred(25,buff);
	    draw_centred(35,"Play Again?");
	    show_screen();
	    if (((PINF>>5) & 0b1) || ((PINF>>6) & 0b1)) {
	    	while ((PINF>>5) & 0b1 || (PINF>>6) & 0b1);
	    	quit = 1;
	    }
	}
	PORTF ^= (1<<PF5);
	PORTF ^= (1<<PF6);
    
    clear_screen();
    setup_menu();
    show_screen();
    //PORTF ^= (1<<PF5);
}

void setup_spacecraft() {
    // max x is 76 max y is 40
    // min x is 1 min y is 9
    init_sprite(&spacecraft, rand() % 74 + 2, rand() % 10 + 10, PLAYER_WIDTH, PLAYER_HEIGHT,player_image);
}

void setup_aliens(void){
  for (unsigned int i = 0; i < MAX_ALIENS - 1; i++) {
    init_sprite(&aliens[i],rand() % 74 + 2, rand() % 20 + 20, 8, 5, alien_image);
    //aliens[i].is_visible = 1;
  }
}

void setup_motherShip() {
    init_sprite(&motherShip, rand() % 74 + 2, rand() % 20 + 20, 10, 10, motherShip_image);
    aliens[MAX_ALIENS - 1] = motherShip;
    aliens[MAX_ALIENS - 1].is_visible = 0;
}

void resetup_aliens(void) {
    for (unsigned int i = 0; i < MAX_ALIENS - 1; i++) {
        aliens[i].x = rand() % 74 + 2;
        aliens[i].y = rand() % 20 + 20;
        aliens[i].is_visible = 1;
    }
}

void update_aliens(void) {
    for (unsigned int i = 0; i < MAX_ALIENS; i++) {     
        if (aliens_move[i] && aliens[i].is_visible == 1) {      
            aliens[i].x += aliens[i].dx;
            aliens[i].y += aliens[i].dy;
        }
    }
}

void aliens_hit_board() {
    for (unsigned int i = 0; i < MAX_ALIENS; i++) {
        if (aliens[i].is_visible == 1) {
            if  ( aliens[i].x <= 1){
                aliens[i].x = 1;
                aliens_move[i] = 0;
            } 
            if (( aliens[i].x + aliens[i].width) >= LCD_X - 1){
                aliens[i].x = LCD_X - 1 - aliens[i].width;
                aliens_move[i] = 0;
            }   
            if (aliens[i].y <= 9) {
                aliens[i].y = 9;
                aliens_move[i] = 0;
            }
            if ( (aliens[i].y + aliens[i].height) >= LCD_Y - 1){
                aliens[i].y = LCD_Y - 1 - aliens[i].height;
                aliens_move[i] = 0;
            }
        }
    }
}

void draw_aliens(void) {
    for (unsigned int i = 0; i < MAX_ALIENS; i++) {
        draw_sprite(&aliens[i]);
    }   
}

void directional_pad_control() {
    // Up
    if ((PIND  >> 1) & 0b1 && spacecraft.y > 9) {
        spacecraft.y--;
        draw_sprite(&spacecraft);
    // Down
    } else if ((PINB  >> 7) & 0b1 && spacecraft.y + spacecraft.height < LCD_Y - 1) {
        spacecraft.y++;
        draw_sprite(&spacecraft);
    // Left
    } else if ((PINB  >> 1) & 0b1 && spacecraft.x > 1) {
        spacecraft.x--;
        draw_sprite(&spacecraft);
    // Right 
    } else if ((PIND  >> 0) & 0b1 && spacecraft.x +spacecraft.width < LCD_X - 1 ) {
        spacecraft.x++;
        draw_sprite(&spacecraft);
    }
}

void setup_missiles(void) {
  for (unsigned int i = 0; i < MAX_MISSILES; i++) {
    init_sprite(&missiles[i], 0,0,2,2,missile_image);
    missiles[i].is_visible = 0;
  }
  init_sprite(&mothership_missile,0,0,2,2,missile_image);
  mothership_missile.is_visible = 0;
}

void launch_missiles(){ 
    unsigned char btns;
    btns = (PINF >> PF5) & 0x03;
    
    if (btns > 0x00) {
        // We have a press, wait until it's done
        while (((PINF >> PF5) & 0x03) > 0x00);
        // Process the press
        if (btns > 0x01) {
            // Left press - change the contrast
            missiles_turn_up();
        } else {
        	missiles_turn_up();
            // Right press - toggle the inversion setting
            
        }
    }
}

void missiles_turn_up() {
    for (unsigned int i = 0; i < MAX_MISSILES;i++) {
        if (missiles[i].is_visible == 0) {
            missiles[i].x = spacecraft.x + 2;
            missiles[i].y = spacecraft.y + 2;
            missiles_dir[i] = direction;
            missiles[i].is_visible = 1;
            break;
        }
    }
}


void dash_attack(int i, int v) {
        float x0 = spacecraft.x + 3, y0 = spacecraft.y + 2;
        float x1 = aliens[i].x + 3, y1 = aliens[i].y + 2;
        float x = abs(x0 - x1), y = abs(y0 - y1);
        float degrees = atanf(y / x);

        if (degrees == 0){
            if (spacecraft.y == 9) {
                degrees += 0.005;
            } else if (spacecraft.y == LCD_Y - 1 - spacecraft.height) {
                degrees -= 0.005;
            }
        }

        float s = sin( degrees ), c = cos( degrees ); 
        float dx = v * 0.1 * c, dy = v * 0.1 * s;
        if (x0 < x1) dx = -dx;
        if (y0 < y1) dy = -dy;
        aliens[i].dx = dx;
        aliens[i].dy = dy;
}

void dash_attack2(int v) {
    
    if (mothership_missile.is_visible == 0 && aliens[MAX_ALIENS-1].is_visible == 1) {
        mothership_missile.is_visible = 1;
        mothership_missile.x = aliens[MAX_ALIENS -1].x + 4;
        mothership_missile.y = aliens[MAX_ALIENS -1].y + 4;   
        float x0 = spacecraft.x + 3, y0 = spacecraft.y + 3;
        float x1 = mothership_missile.x, y1 = mothership_missile.y;
        float x = abs(x0 - x1), y = abs(y0 - y1);
        float degrees = atanf(y / x);
        float s = sin( degrees ), c = cos( degrees ); 
        float dx = v * 0.1 * c, dy = v * 0.1 * s;
        if (x0 < x1) dx = -dx;
        if (y0 < y1) dy = -dy;
        mothership_missile.dx = dx;
        mothership_missile.dy = dy;
    }
}

void update_missiles(void) {
    for (unsigned int i = 0; i < MAX_MISSILES; i++) {
        if (missiles[i].is_visible == 1) {
            missiles[i].x += MISSILE_VELOCITY * cos (missiles_dir[i]);
            missiles[i].y += MISSILE_VELOCITY * sin (missiles_dir[i]);

            if (missiles[i].x <= 1 || 
                missiles[i].x + missiles[i].width >= LCD_X  ||
                missiles[i].y <= 9 || 
                missiles[i].y + missiles[i].height >= LCD_Y) {
                missiles[i].is_visible = 0;
            }   
        }
    }

    if (mothership_missile.is_visible == 1) {
        mothership_missile.x += mothership_missile.dx;
        mothership_missile.y += mothership_missile.dy;
        
        if (mothership_missile.x <= 1 || 
            mothership_missile.x + mothership_missile.width >= LCD_X  ||
            mothership_missile.y <= 9 || 
            mothership_missile.y + mothership_missile.height >= LCD_Y) {
            mothership_missile.is_visible = 0;
        } 
    }
}

void draw_missiles() {
    for (unsigned i = 0; i < MAX_MISSILES;i++){
        draw_sprite(&missiles[i]);   
    }
    draw_sprite(&mothership_missile);
}

double get_system_time() {
    //return TCNT1*PRESCALER/FREQUENCY;
    return (ovf_count * 65536 + TCNT1) * PRESCALER / FREQUENCY;
}

void check_collision() {
    for (unsigned int i = 0; i < MAX_ALIENS - 1;i++){
        for (unsigned int j = 0; j < MAX_MISSILES;j++) {
            if (sprites_collided(aliens[i],missiles[j]) &&
                aliens[i].is_visible == 1 &&
                missiles[j].is_visible == 1) {
                aliens[i].is_visible = 0;
                missiles[j].is_visible = 0;
                scores++;
                send_debug_string("An alien was killed and player got one score");
            }
        }
    }
}


void check_collision3() {
    
    for (unsigned int i = 0; i < MAX_MISSILES;i++) {
        if (sprites_collided2(aliens[MAX_ALIENS - 1],missiles[i]) &&
            aliens[MAX_ALIENS -1].is_visible == 1 &&
            missiles[i].is_visible == 1) {
            
            if (health_pool <= 1) {
                aliens[MAX_ALIENS -1].is_visible = 0;
                missiles[i].is_visible = 0;
                scores+=10;
                resetup_aliens();
                mothership_fight = 2;
                send_debug_string("Mothership was killed and player got ten scores");
            } else if (health_pool > 8){  
                health_pool--;
                missiles[i].is_visible = 0;
                motherShip_image[19] &= ~(1<<15 - health_pool);
                motherShip.bitmap = motherShip_image;
            } else if (health_pool <= 8) {
                health_pool--;
                missiles[i].is_visible = 0;
                motherShip_image[18] &= ~(1<< 7 - health_pool);
                motherShip.bitmap = motherShip_image;               
            }
        }
    }   

    if (sprites_collided(aliens[MAX_ALIENS -1],spacecraft) &&
        aliens[MAX_ALIENS -1].is_visible == 1 ||
        sprites_collided(mothership_missile,spacecraft)) {
        // aliens[MAX_ALIENS -1].x = rand() % 74 + 2;
        // aliens[MAX_ALIENS - 1].y = rand() % 20 + 20;
        lives--;
        spacecraft.x = rand() % 74 + 2;
        spacecraft.y = rand() % 10 + 10;
        mothership_missile.is_visible = 0;
        send_debug_string("Player was killed by the mothership and lost a life");
    }  
}

void check_collision2() {
    for (unsigned int i = 0; i < MAX_ALIENS - 1;i++){
        if (sprites_collided2(aliens[i],spacecraft) &&
            aliens[i].is_visible == 1) {
            //aliens[i].is_visible = 0;
            lives--;
            spacecraft.x = rand() % 74 + 2;
            spacecraft.y = rand() % 10 + 10;
            send_debug_string("Player was killed by an alien and lost a life");
        }       
    }
}

int sprites_collided(Sprite sprite_1, Sprite sprite_2) {
    double sprite_1_top = sprite_1.y,
        sprite_1_bottom = sprite_1_top + sprite_1.height - 1,
        sprite_1_left = sprite_1.x,
        sprite_1_right = sprite_1_left + sprite_1.width - 1;

    double sprite_2_top = sprite_2.y,
        sprite_2_bottom = sprite_2_top + sprite_2.height - 1,
        sprite_2_left = sprite_2.x,
        sprite_2_right = sprite_2_left + sprite_2.width - 1;

    return !(
        sprite_1_bottom < sprite_2_top
        || sprite_1_top > sprite_2_bottom
        || sprite_1_right < sprite_2_left
        || sprite_1_left > sprite_2_right
        );
}

int sprites_collided2(Sprite sprite_1, Sprite sprite_2) {
    double sprite_1_top = sprite_1.y,
        sprite_1_bottom = sprite_1_top + sprite_1.height - 1 - 2,
        sprite_1_left = sprite_1.x,
        sprite_1_right = sprite_1_left + sprite_1.width - 1;

    double sprite_2_top = sprite_2.y,
        sprite_2_bottom = sprite_2_top + sprite_2.height - 1,
        sprite_2_left = sprite_2.x,
        sprite_2_right = sprite_2_left + sprite_2.width - 1;

    return !(
        sprite_1_bottom < sprite_2_top
        || sprite_1_top > sprite_2_bottom
        || sprite_1_right < sprite_2_left
        || sprite_1_left > sprite_2_right
        );
}

void aliens_rematerialise() {
    int something_visible = 0;
    for (unsigned int i = 0; i < MAX_ALIENS - 1;i++) {
        if (aliens[i].is_visible == 1){
            something_visible = 1;
        }
    }
    
    if (!something_visible && !mothership_fight) {
    	aliens[MAX_ALIENS - 1].is_visible = 1;
    	motherShip_image[18] = 0b11111111;
        motherShip_image[19] = 0b11000000; 
    	health_pool = 10;
    	mothership_fight = 1;
        //resetup_aliens();
    }

    if (!something_visible && mothership_fight == 2) {
        resetup_aliens();
    }
}

void send_debug_string(char* string) {
     // Send the debug preamble...
     usb_serial_write("[DEBUG @ ", 10);
     char buff[20];
     
     if (get_system_time() < 100) {
        sprintf(buff, "0%06.3f] ", get_system_time());
     } else {
        sprintf(buff, "%06.3f] ", get_system_time());
     }
     usb_serial_write(buff, 9);
     // Send all of the characters in the string
     unsigned char char_count = 0;
     while (*string != '\0') {
         usb_serial_putchar(*string);
         string++;
         char_count++;
     }

     // Go to a new line (force this to be the start of the line)
     usb_serial_putchar('\r');
     usb_serial_putchar('\n');
}

void send_spacecraft_information() {
    
    usb_serial_write("[DEBUG @ ", 10);
    char buff[20];
    char buff1[50];
    //char buff2[20];
    if (get_system_time() < 100) {
    sprintf(buff, "0%06.3f] ", get_system_time());
    } else {
    sprintf(buff, "%06.3f] ", get_system_time());
    }
    usb_serial_write(buff, 9);
    // Send all of the characters in the string
    unsigned char char_count = 0;   
    // sprintf(buff1, "%04.1f,%04.1f)", spacecraft.x, spacecraft.y);
    sprintf(buff1, "Spacecraft is at (%04.1f,%04.1f) aiming (degree) = %006.3f",
    				 spacecraft.x, spacecraft.y, read_adc(1) / (1023.00/720.00));
    usb_serial_write(buff1, 55);


    // Go to a new line (force this to be the start of the line)
    usb_serial_putchar('\r');
    usb_serial_putchar('\n');
}

void send_debug_string1(char* string) {
     // Send the debug preamble...
     // Send all of the characters in the string
     unsigned char char_count = 0;
     while (*string != '\0') {
         usb_serial_putchar(*string);
         string++;
         char_count++;
     }

     // Go to a new line (force this to be the start of the line)
     usb_serial_putchar('\r');
     usb_serial_putchar('\n');
}

void enter_breakpoint() {
    // Implement this so that it correctly pauses the code and displays the message
    // over serial. NOTHING except the serial connection and the incrementing of
    // ovf_count should still be happening.
    if (usb_serial_available()) {
        int16_t curr_char = usb_serial_getchar();
        
        if (curr_char == 'a' && spacecraft.x > 1) {
            spacecraft.x--;
        } else if (curr_char == 'd' && spacecraft.x + spacecraft.width < LCD_X - 1) {
            spacecraft.x++;
        } else if (curr_char == 'w' && spacecraft.y > 9) {
            spacecraft.y--;
        } else if (curr_char == 's' && spacecraft.y + spacecraft.height < LCD_Y - 1) {
            spacecraft.y++;
        } else if (curr_char == ' ') {
            missiles_turn_up();
        }
    }   
}

void draw_centred(unsigned char y, char* string) {
    // Draw a string centred in the LCD when you don't know the string length
    unsigned char l = 0, i = 0;
    while (string[i] != '\0') {
        l++;
        i++;
    }
    char x = 42-(l*5/2);
    draw_string((x > 0) ? x : 0, y, string);
}

int16_t read_adc(int8_t pin_number){
  // Define the input pin from the MUX
  int16_t my_adc_value;
  ADMUX  &= 0xf8;  // 0xf8 0b1111 1000
  ADMUX  |= pin_number;   // 0b0000 0001

  // Start the conversion for the ADC
  ADCSRA |= 0b1 << ADSC;

  // Wait for the ADC to finished
  while ((ADCSRA>>ADSC) & 0b1);

  // Now we can get the value from ADC
  my_adc_value  = ADC;
  return my_adc_value;
}

void aiming_system(void) {
    int length = 7;
    float x = spacecraft.x + 3;
    float y = spacecraft.y + 3;
    my_adc_value = read_adc(1); 
    float degree = (my_adc_value) / (1023.00/720.00);
    float radians = degree * M_PI / 180;
    float x1 = x + length * cos (radians);
    float y1 = y + length * sin (radians);
    // max x is 76 max y is 40
    // min x is 1 min y is 9
    if (x1 >= 1 && x1 <= LCD_X - 1 && y1 >= 9 && y1 <= LCD_Y - 1) {
        draw_line(x,y,x1,y1);
    }

    direction = radians;
}
// Set the interrupt service routine for timer 0
ISR(TIMER1_OVF_vect){
    ovf_count++;
    seconds+=0.5;

    if (seconds == 60) {
        seconds = 0;
        minutes++;
    }
   
    for (unsigned int i = 0; i < MAX_ALIENS;i++) {
        if (fmod(seconds+4,aliens_move_to[i])==0.0f && aliens_move[i] == 0) {
            dash_attack(i,ALIEN_VELOCITY);
            aliens_move[i] = 1;
        }
    }

    if (fmod(seconds,2)==0.0f && aliens_move[MAX_ALIENS - 1] == 0) {
        dash_attack(MAX_ALIENS - 1, MOTHERSHIP_VELOCITY);
        aliens_move[MAX_ALIENS - 1] = 1;
    }

    if (fmod(seconds,2)==0.0f) {
    	dash_attack2(MISSILE_VELOCITY*10);
    }

    if (display_info)
    send_spacecraft_information();
}
